#include "prc.h"


/*==============================================================================
** Function...: menu
** Return.....: void
** Description: Terminal menu
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void menu(uint16_t argc, char *argv[])
{
    char s[200];
  
    if (argc) {
        if (strcmp(argv[0], "help") == 0) {
          // list the commands supported
          uart_print("The following commands are supported:\n");
          uart_print("Version\n");
          uart_print("spiDebug on/Off \n");
          uart_print("printReg simple/detail "
                  "# prints all register values for RFM69HW\n");
          uart_print("temp\n");
          uart_print("rssi\n");
        }
        else if (strcmp(argv[0], "printReg") == 0){
            if(argc>1){
                if (0 == strcmp (argv[1], "simple"))
                {
                    printHeader("Reg print simple");
                    simpleRegPrint();
                }
                else if (0 == strcmp (argv[1], "detail"))
                {
                    printHeader("Detailed Register Print");
                    detailRegPrint();
                }
                else
                {
                    sprintf(s, "Error: unknown cmd: %s\n", argv[1]);
                    uart_print(s);
                    uart_print("printReg simple/detail # prints all "
                            "regvalues of RFM69HW \n");
                }
            }
            else
            {
                uart_print("Error: argument missing\n");
                uart_print("printReg simple/detail # prints all "
                        "regvalues of RFM69HW \n");
            }
        }
        else if (strcmp(argv[0], "temp") == 0){
               int8_t t, ret=255;
               ret = rf69_read_temp(&t);
               sprintf(s,"Temp %d (%d)\n",t, ret);
               uart_print(s);   
        }
        else if (strcmp(argv[0], "clear_fifo") == 0){
            _rf69_clear_fifo();
        }
            
        else if (strcmp(argv[0], "rssi") == 0){

            int16_t val;
            _rf69_sample_rssi(&val);
            sprintf(s,"RSSI %d\n",val);
            uart_print(s);  
        }
        
        else if (strcmp(argv[0], "get_mode") == 0){
            uint8_t val;   
            int capVal;
            _rf69_read(RFM69_REG_01_OPMODE, &val);
            
             capVal = (val >> 2) & 0x7;
             
            if ( capVal == 0b000 ) {
                uart_print( "000 -> Sleep mode (SLEEP)\n" );
            } else if ( (capVal = 0b001) == 1 ) {
                uart_print( "001 -> Standby mode (STDBY)\n" );
            } else if ( (capVal = 0b010) == 1 ) {
                uart_print ( "010 -> Frequency Synthesizer mode (FS)\n" );
            } else if ( (capVal = 0b011) == 1 ) {
                uart_print ( "011 -> Transmitter mode (TX)\n" );
            } else if ( (capVal = 0b100) == 1 ) {
                uart_print ( "100 -> Receiver Mode (RX)\n" );
            } else {
            sprintf(s, "%13s\n", byte_to_binary(capVal));
            uart_print(s);
            }        
        }
        
        else if (strcmp(argv[0], "send") == 0)
        {
             uint8_t data[255];
             uart_print("sending");
             rf69_send(data, 4, 5);
        }
        
        else {
          sprintf(s, "Error: unknown cmd: %s\n", argv[0]);
          uart_print(s);
        }
    }
}
#include "prc.h"



/*==============================================================================
** Function...: byte_to_binary
** Return.....: Binary string representation
** Description: Converts a byte into binary string
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
const char *byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';
    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }
    return b;
}


/*==============================================================================
** Function...: printHeader
** Return.....: void 
** Description: Welcome msg
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void printHeader(void* msg)
{
  int n;
  int size = strlen(msg);
  uart_print("\n\n\n");
  for (n=0; n<size*3; n++)
  {
    printf("*");
  }
  uart_print("\n");
  uart_print("*");
  for (n=0; n<size; n++)
  {
    uart_print(" ");
  }
  uart_print(msg);
  for (n=0; n<(size*2)-2; n++)
  {
    uart_print(" ");
  }
  
  uart_print("*");
  uart_print("\n");
  
  for (n=0; n<size*3; n++)
  {
    uart_print("*");
  }
  uart_print("\n\n");
}


/*==============================================================================
** Function...: simpleRegPrint
** Return.....: void
** Description: Print the register value of RFM69HW
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void simpleRegPrint(void)
{
    char s[200];
    uint8_t regAddr;
    
    uart_print("Address    HEX \t      BIN\n");
    for (regAddr = 1; regAddr <= 0x4F; regAddr++)
    {
      uint8_t res;
      _rf69_read(regAddr, &res);
      sprintf(s,"   0x%02x          0x%02x ",regAddr, res);
      uart_print(s);
      sprintf(s, "%13s\n", byte_to_binary(res));
      uart_print(s);
    }
}


/*==============================================================================
** Function...: detailRegPrint
** Return.....: void
** Description: Print detail register values of RFM69HW
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void detailRegPrint(void)
{
    char s[200];
    uint8_t regAddr;
    int capVal;
    
    //... State Variables for intelligent decoding
    uint8_t modeFSK = 0;
    int bitRate = 0;
    int freqDev = 0;
    long freqCenter = 0;
    
    for (regAddr = 1; regAddr <= 0x4F; regAddr++)
    {
        uint8_t regVal;
        _rf69_read(regAddr, &regVal);
        
        sprintf(s, ">>>  Adress 0x%x | Value 0x%x | Bin %s\n", 
                regAddr, regVal, byte_to_binary(regVal));
      
        switch ( regAddr ) 
        {
            case 0x1 : {
                uart_print(s);
                uart_print( " RegOpMode [B7-0] (0x01) \n" );
                uart_print( "[B7] Controls the automatic Sequencer "
                        "(see section 4.2) \n\tSequencer : " );
                if ( 0x80 & regVal ) {
                    uart_print( "1 -> Mode is forced by the user\n" );
                } else {
                    uart_print( "0 -> Operating mode as selected with Mode bits"
                           "\n\t in RegOpMode is automatically reached with the"
                           " Sequencer\n" );
                }

                uart_print( "\n[B6] Enables Listen mode, should be enabled "
                        "whilst in Standby mode:\n\tListenOn : " );
                if ( 0x40 & regVal ) {
                    uart_print( "1 -> On\n" );
                } else {
                    uart_print( "0 -> Off ( see section 4.3)\n" );
                }

                uart_print( "\n[B5] Aborts Listen mode when set together with "
                        "ListenOn=0 \n See section 4.3.4 for details "
                        "(Always reads 0.)\n" );
                if ( 0x20 & regVal ) {
                    uart_print( "ERROR - ListenAbort should NEVER return 1 "
                            "this is a write only register\n" );
                }

                uart_print("\n[B4-2] Transceiver's operating modes:\n\tMode : ");
                capVal = (regVal >> 2) & 0x7;
                if ( capVal == 0b000 ) {
                    uart_print( "000 -> Sleep mode (SLEEP)\n" );
                } else if ( (capVal = 0b001) ) {
                    uart_print( "001 -> Standby mode (STDBY)\n" );
                } else if ( (capVal = 0b010) ) {
                    uart_print ( "010 -> Frequency Synthesizer mode (FS)\n" );
                } else if ( (capVal = 0b011) ) {
                    uart_print ( "011 -> Transmitter mode (TX)\n" );
                } else if ( (capVal = 0b100) ) {
                    uart_print ( "100 -> Receiver Mode (RX)\n" );
                } else {
                    sprintf(s, "%13s\n", byte_to_binary(capVal));
                    uart_print(s);
                    uart_print( " -> RESERVED\n" );
                }
                uart_print( "\n[B1-0] Unused\n" );
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0x2 : {
                uart_print(s);
                uart_print( " RegDataModul [B7-0] (0x02) \n" );
                uart_print( "[B7] Unused\n" );
                uart_print("[B6-5] Data Processing mode:\nDataMode : ");
                capVal = (regVal >> 5) & 0x3;
                if ( capVal == 0b00 ) {
                    uart_print( "00 -> Packet mode\n" );
                } else if ( capVal == 0b01 ) {
                    uart_print( "01 -> reserved\n" );
                } else if ( capVal == 0b10 ) {
                    uart_print( "10 -> Continuous mode with bit synchronizer\n" );
                } else if ( capVal == 0b11 ) {
                    uart_print( "11 -> Continuous mode without bit synchronizer\n" );
                }

                uart_print("\n[B4-3] Modulation scheme:\nModulation Type : ");
                capVal = (regVal >> 3) & 0x3;
                if ( capVal == 0b00 ) {
                    uart_print( "00 -> FSK\n" );
                    modeFSK = 1;
                } else if ( capVal == 0b01 ) {
                    uart_print( "01 -> OOK\n" );
                } else if ( capVal == 0b10 ) {
                    uart_print ( "10 -> reserved\n" );
                } else if ( capVal == 0b11 ) {
                    uart_print( "11 -> reserved\n" );
                }
                
                uart_print( "\n[B2] Unused\n" );
                
                uart_print("\n[B1-0] Data shaping: ");
                if ( modeFSK ) {
                    uart_print( "in FSK:\n" );
                } else {
                    uart_print( "in OOK:\n" );
                }
                uart_print("ModulationShaping : ");
                capVal = regVal & 0x3;
                if ( modeFSK ) {
                    if ( capVal == 0b00 ) {
                        uart_print( "00 -> no shaping\n" );
                    } else if ( capVal == 0b01 ) {
                        uart_print( "01 -> Gaussian filter, BT = 1.0\n" );
                    } else if ( capVal == 0b10 ) {
                        uart_print( "10 -> Gaussian filter, BT = 0.5\n" );
                    } else if ( capVal == 0b11 ) {
                        uart_print( "11 -> Gaussian filter, BT = 0.3\n" );
                    }
                } else {
                    if ( capVal == 0b00 ) {
                        uart_print( "00 -> no shaping\n" );
                    } else if ( capVal == 0b01 ) {
                        uart_print( "01 -> filtering with f(cutoff) = BR\n" );
                    } else if ( capVal == 0b10 ) {
                        uart_print( "10 -> filtering with f(cutoff) = 2*BR\n" );
                    } else if ( capVal == 0b11 ) {
                        uart_print( "ERROR - 11 is reserved\n" );
                    }
                }
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0x3 : {
                uart_print(s);
                bitRate = (regVal << 8);

                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0x4 : {
                uart_print(s);
                uart_print( " RegBitrateLsb [B7-0] (0x04) \n" );
                bitRate |= regVal;
                uart_print ( "[B7-0] LSB of Bit Rate  : ");
                unsigned long val = 32UL * 1000UL * 1000UL / bitRate;
                sprintf(s,"\n\tBitRate: %lu bps",val);
                uart_print( s );
                uart_print("\n");
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
                
            case 0x5 : {
                uart_print(s);
                freqDev = ( (regVal & 0x3f) << 8 );
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0x6 : {
                uart_print(s);
                uart_print( " RegFdevLsb [B7-0] (0x06) \n" );
                freqDev |= regVal;
                uart_print( "Frequency deviation\n\tFdev : " );
                unsigned long val = 61UL * freqDev;
                sprintf(s," %lu hz", val);
                uart_print( s );
                uart_print("\n");
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            
            
            case 0x7 : {
                uart_print(s);
                unsigned long tempVal = regVal;
                freqCenter = ( tempVal << 16 );
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0x8 : {
                uart_print(s);
                unsigned long tempVal = regVal;
                freqCenter = freqCenter | ( tempVal << 8 );
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0x9 : {
                uart_print(s);
                uart_print( " RegFrfMid [B7-0] (0x09) \n" );
                freqCenter = freqCenter | regVal;
                uart_print ( "RF Carrier frequency\n\t FRF : " );
                unsigned long val = (61UL * freqCenter)/1000000.0;
                sprintf(s," %lu MHz", val);
                uart_print( s );
                uart_print("\n");
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }  
            
            case 0xa : {
                uart_print(s);
                uart_print( " RegFrfLsb [B7-0] (0x0A) \n" );
                uart_print ( "RC calibration control & status\nRcCalDone:\n\t" );
                if ( 0x40 & regVal ) {
                    uart_print ( "1 -> RC calibration is over\n" );
                } else {
                    uart_print ( "0 -> RC calibration is in progress\n" );
                }
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0xb : {
                uart_print(s);
                uart_print( " RegOsc1 [B7-0] (0x0B) \n" );
                uart_print ( "Improved AFC routine for signals with modulation "
                        "index \n lower than 2.  Refer to section 3.4.16 for "
                        "details\nAfcLowBetaOn : \n\t" );
                if ( 0x20 & regVal ) {
                    uart_print( "1 -> Improved AFC routine\n" );
                } else {
                    uart_print( "0 -> Standard AFC routine\n" );
                }
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0xc : {
                uart_print(s);
                uart_print( " RegOsc1 [B7-0] (0x0C) \n" );
                uart_print ( "Reserved\n\n" );
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            case 0xd : {
                uart_print(s);
                uart_print( " RegListen1 [B7-0] (0x0D) \n" );
                uint8_t val;
                uart_print ( "[B7-6]Resolution of Listen mode Idle time "
                        "(calibrated RC osc):\n\tListenResolIdle : " );
                val = regVal >> 6;
                if ( val == 0b00 ) {
                    uart_print ( "00 -> reserved\n" );
                } else if ( val == 0b01 ) {
                    uart_print ( "01 -> 64 us\n" );
                } else if ( val == 0b10 ) {
                    uart_print ( "10 -> 4.1 ms\n" );
                } else if ( val == 0b11 ) {
                    uart_print ( "11 -> 262 ms\n" );
                }

                uart_print ( "\n[B5-4]Resolution of Listen mode Rx time "
                        "(calibrated RC osc):\n\tListenResolRx : " );
                val = (regVal >> 4) & 0x3;
                if ( val == 0b00 ) {
                    uart_print ( "00 -> reserved\n" );
                } else if ( val == 0b01 ) {
                    uart_print ( "01 -> 64 us\n" );
                } else if ( val == 0b10 ) {
                    uart_print ( "10 -> 4.1 ms\n" );
                } else if ( val == 0b11 ) {
                    uart_print ( "11 -> 262 ms\n" );
                }

                uart_print ( "\n[B3]Criteria for packet acceptance in "
                        "Listen mode: \n\tListenCriteria : " );
                if ( 0x8 & regVal ) {
                    uart_print ( "1 -> signal strength is above RssiThreshold "
                            "and SyncAddress matched\n" );
                } else {
                    uart_print ( "0 -> signal strength is above RssiThreshold\n" );
                }

                uart_print ( "\n[B2-1]Action taken after acceptance of a packet "
                        "in Listen mode:\n\tListenEnd : " );
                val = (regVal >> 1 ) & 0x3;
                if ( val == 0b00 ) {
                    uart_print ( "00 -> chip stays in Rx mode. Listen mode stops"
                            " and must be disabled (see section 4.3)\n" );
                } else if ( val == 0b01 ) {
                    uart_print ( "01 -> chip stays in Rx mode until PayloadReady"
                            "\n or Timeout interrupt occurs. It then goes to the"
                            " mode defined by Mode.\nListen mode stops and must "
                            "be disabled (see section 4.3)\n" );
                } else if ( val == 0b10 ) {
                    uart_print ( "10 -> chip stays in Rx mode until PayloadReady "
                            "\n or Timeout occurs.  Listen mode then resumes in "
                            "Idle state.  FIFO content is lost at next "
                            "Rx wakeup.\n" );
                } else if ( val == 0b11 ) {
                    uart_print ( "11 -> Reserved\n" );
                }
                uart_print( "-------------------------------------------------"
                        "--------------------------------------"
                        "\n" );
                break;
            }
            
            default : {
            }
        }
    }
}
/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: main.c
        Version:   v0.0
        Date:      21-05-2015
    ============================================================================
 */

/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/

#include <p24FJ64GB002.h>

#include "main.h"
#include "rfm69/ukhasnet-rfm69.h"



/*
** =============================================================================
**                   LOCAL  FUNCTION DECLARATIONS
** =============================================================================
*/

void systemInit(void);


/**
 * Description: Main Application layer, each application must have this function 
 * @param:     App tasks: incoming and  outgoing task
 * @return:    If an APP  is defined then we are ready to run in this case
 *             this function will return APP_CONF_SUCCESS, else APP_CONF_NOT_SET
*/



/*============================================================================*/
/*                           MCU CONFIGURATION                                */
/*============================================================================*/
// CONFIG1
#pragma config WDTPS = PS256   // Watchdog Timer Postscaler (1:32,768)
#pragma config FWPSA = PR128   // WDT Prescaler (Prescaler ratio of 1:128)
#pragma config WINDIS = OFF    // Windowed WDT (Standard Watchdog Timer enabled,(Windowed-mode is disabled))
#pragma config FWDTEN = OFF    // Watchdog Timer (Watchdog Timer is disabled)
#pragma config ICS = PGx1      // Emulator Pin Placement Select bits (Emulator functions are shared with PGEC1/PGED1)
#pragma config GWRP = OFF      // General Segment Write Protect (Writes to program memory are allowed)
#pragma config GCP = OFF       // General Segment Code Protect (Code protection is disabled)
#pragma config JTAGEN = OFF    // JTAG Port Enable (JTAG port is disabled)

// CONFIG2
 #pragma config POSCMOD = NONE // Primary Oscillator is disabled
 #pragma config I2C1SEL = PRI  // I2C1 Pin Select bit (Use default SCL1/SDA1 pins for I2C1 )
 #pragma config IOL1WAY = OFF  // IOLOCK One-Way Set Disabled (Once set, the IOLOCK bit cannot be cleared)
 #pragma config OSCIOFNC = OFF // OSCO Pin Configuration (OSCO pin functions as clock output (CLKO))
 #pragma config FCKSM = CSDCMD // Clock Switching and Fail-Safe Clock Monitor (Sw Disabled, Mon Disabled)
 #pragma config FNOSC = FRCPLL // Fast RC Oscillator with Postscaler and PLL module
 #pragma config PLL96MHZ = ON  // 96MHz PLL Startup Select (96 MHz PLL Startup is enabled automatically on start-up)
 #pragma config PLLDIV = DIV2  // USB 96 MHz PLL Prescaler Select (Oscillator input divided by 2 (8 MHz input))
 #pragma config IESO = OFF     // Internal External Switchover (IESO mode (Two-Speed Start-up) enabled)

 // CONFIG3
 #pragma config WPFP = WPFP0     // Write Protection Flash Page Segment Boundary (page 0)
 #pragma config SOSCSEL = IO     // Secondary Oscillator Pin Mode Select (SOSC pins in Default (high drive-strength) Oscillator Mode)
 #pragma config WUTSEL = LEG     // Voltage Regulator Wake-up Time Select (Default regulator start-up time used)
 #pragma config WPDIS = WPDIS    // Segment Write Protection Disable (Segmented code protection disabled)
 #pragma config WPCFG = WPCFGDIS // Write Protect Configuration Page Select (Last page and Flash Configuration words are unprotected)
 #pragma config WPEND = WPENDMEM // Segment Write Protection End Page Select (Write Protect from WPFP to the last page of memory)

// CONFIG4
 #pragma config DSWDTPS = DSWDTPS0 // DSWDT Postscale Select 8.5 sec)
 #pragma config DSWDTOSC = LPRC    // Deep Sleep Watchdog Timer Oscillator Select (DSWDT uses Low Power RC Oscillator (LPRC))
 #pragma config RTCOSC = LPRC      // RTCC uses Low Power RC Oscillator (LPRC)
 #pragma config DSBOREN = OFF      // Deep Sleep BOR Enable bit (BOR disabled in Deep Sleep)
 #pragma config DSWDTEN = ON       // Deep Sleep Watchdog Timer (DSWDT enabled) 



/*==============================================================================
** Function...: main
** Return.....: int
** Description: Main function (OS)
** Created....: 22.04.2013 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int main(void) 
{
    systemInit();
    

    pic24_assert(1<2);
    os();

    
    
    return 0;
}


/*==============================================================================
 ** Function...: systemInit
 ** Return.....: void
 ** Description: Main init function
 ** Created....: 25.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void systemInit(void)
{
    AD1PCFGL     = 0xFFFF; // Set to all digital I/O
    
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB7 = 0; // Set B7 as SPi CSN (OUTPUT))
    
    UART1Init();
    uart_print("UART Initialized\n");
    
    uart_print("Initializing Time Driver .........");
    timer();
    uart_print("Done\n");
    
    uart_print("Initializing SPI Driver .........");
    spi();
    uart_print("Done\n");
    
    uart_print("Initializing RFM69HW Driver .........");
    rf69_init();
    uart_print("Done\n");
    
    uart_print("System initialization complete\n\n");

}

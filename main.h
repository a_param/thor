/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: includes.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef MAIN_H
#define	MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "spi/spi.h"
#include "uart/pic24_uart.h"
#include "timer/time.h"
#include "terminal/terminal.h"
#include "rfm69/ukhasnet-rfm69.h"
#include "prc/prc.h"
#include "utility/pic24_assert.h"
#include "os/os.h"


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

void detailRegPrint(void);
const char *byte_to_binary(int x);
void detailRegPrint(void);

void simpleRegPrint(void);
void printHeader(void* msg);
/*
** ==========================================================================
**                        DEFINES AND MACROS
** ==========================================================================
*/


 /* ********************** LEDs *********************** */
#define LED_GREEN_1 LATBbits.LATB2
#define LED_GREEN_1_TOGGLE LED_GREEN_1   = ~LED_GREEN_1

//

#endif	/* MAIN_H */